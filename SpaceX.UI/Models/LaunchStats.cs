﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Runtime.Serialization;
using System.ComponentModel.DataAnnotations;

namespace SpaceX.UI.Models
{
    [DataContract]
    public class Core
    {
        [DataMember]
        public string core_serial { get; set; }
        [DataMember]
        public int flight { get; set; }
        [DataMember]
        public int? block { get; set; }
        [DataMember]
        public bool reused { get; set; }
        [DataMember]
        public bool? land_success { get; set; }
        [DataMember]
        public string landing_type { get; set; }
        [DataMember]
        public string landing_vehicle { get; set; }
    }

    [DataContract]
    public class FirstStage
    {
        [DataMember]
        public List<Core> cores { get; set; }
    }

    [DataContract]
    public class Payload
    {
        [DataMember]
        public string payload_id { get; set; }
        [DataMember]
        public bool reused { get; set; }
        [DataMember]
        public List<string> customers { get; set; }
        [DataMember]
        public string payload_type { get; set; }
        [DataMember]
        public int? payload_mass_kg { get; set; }
        [DataMember]
        public double? payload_mass_lbs { get; set; }
        [DataMember]
        public string orbit { get; set; }
        [DataMember]
        public string cap_serial { get; set; }
        [DataMember]
        public double? mass_returned_kg { get; set; }
        [DataMember]
        public int? mass_returned_lbs { get; set; }
        [DataMember]
        public int? flight_time_sec { get; set; }
        [DataMember]
        public string cargo_manifest { get; set; }
    }

    [DataContract]
    public class SecondStage
    {
        [DataMember]
        public List<Payload> payloads { get; set; }
    }

    [DataContract]
    public class Rocket
    {
        [DataMember]
        public string rocket_id { get; set; }
        [DataMember]
        public string rocket_name { get; set; }
        [DataMember]
        public string rocket_type { get; set; }
        [DataMember]
        public FirstStage first_stage { get; set; }
        [DataMember]
        public SecondStage second_stage { get; set; }
    }

    [DataContract]
    public class Telemetry
    {
        [DataMember]
        public string flight_club { get; set; }
    }

    [DataContract]
    public class Reuse
    {
        [DataMember]
        public bool core { get; set; }
        [DataMember]
        public bool side_core1 { get; set; }
        [DataMember]
        public bool side_core2 { get; set; }
        [DataMember]
        public bool fairings { get; set; }
        [DataMember]
        public bool capsule { get; set; }
    }

    [DataContract]
    public class LaunchSite
    {
        [DataMember]
        public string site_id { get; set; }
        [DataMember]
        public string site_name { get; set; }
        [DataMember]
        public string site_name_long { get; set; }
    }

    [DataContract]
    public class Links
    {
        [DataMember]
        public string mission_patch { get; set; }
        [DataMember]
        public string mission_patch_small { get; set; }
        [DataMember]
        public string article_link { get; set; }
        [DataMember]
        public string video_link { get; set; }
        [DataMember]
        public string presskit { get; set; }
        [DataMember]
        public string reddit_campaign { get; set; }
        [DataMember]
        public string reddit_launch { get; set; }
        [DataMember]
        public string reddit_recovery { get; set; }
        [DataMember]
        public string reddit_media { get; set; }
    }

    [DataContract]
    public class RootObject
    {
        [DataMember]
        public int flight_number { get; set; }
        [DataMember]
        public string mission_name { get; set; }
        [DataMember]
        public string launch_year { get; set; }
        [DataMember]
        public int launch_date_unix { get; set; }
        [DataMember]
        [DisplayFormat(DataFormatString ="{0:s}")]
        public DateTime launch_date_utc { get; set; }
        [DataMember]
        public DateTime launch_date_local { get; set; }
        [DataMember]
        public Rocket rocket { get; set; }
        [DataMember]
        public Telemetry telemetry { get; set; }
        [DataMember]
        public Reuse reuse { get; set; }
        [DataMember]
        public LaunchSite launch_site { get; set; }
        [DataMember]
        public bool launch_success { get; set; }
        [DataMember]
        public Links links { get; set; }
        [DataMember]
        public string details { get; set; }
    }
}