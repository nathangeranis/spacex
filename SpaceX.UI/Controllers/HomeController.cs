﻿using Newtonsoft.Json;
using SpaceX.UI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace SpaceX.UI.Controllers
{
    public class HomeController : Controller
    {

        string baseurl = "https://api.spacexdata.com/";
        public ActionResult Index()
        {
            List<RootObject> stats = new List<RootObject>();
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(baseurl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue
                    ("application/json"));
                HttpResponseMessage response = client.GetAsync("v2/launches").Result;
                if (response.IsSuccessStatusCode)
                {
                    var statsResp = response.Content.ReadAsStringAsync().Result;
                    stats = JsonConvert.DeserializeObject<List<RootObject>>(statsResp);
                }
            }
            return View(stats);
        }
    }
}